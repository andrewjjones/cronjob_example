#include <mgos.h>
#include "mgos_gpio.h"
#include "mgos_crontab.h"

#define PREC_LED_GPIO 2

void ledoff(struct mg_str action, struct mg_str payload, void *userdata) {
  mgos_gpio_write(PREC_LED_GPIO, 0);
  (void) payload;
  (void) userdata;
  (void) action;
}

void ledon(struct mg_str action, struct mg_str payload, void *userdata) {
  mgos_gpio_write(PREC_LED_GPIO, 1);
  (void) payload;
  (void) userdata;
  (void) action;
}

enum mgos_app_init_result mgos_app_init(void) {
  /* Set LED_GPIOs as output */
  mgos_gpio_set_mode(PREC_LED_GPIO, MGOS_GPIO_MODE_OUTPUT);

  /* Register crontab handler - LED OFF */
  mgos_crontab_register_handler(mg_mk_str("ledoff"), ledoff, NULL);

  /* Register crontab handler - LED ON */
  mgos_crontab_register_handler(mg_mk_str("ledon"), ledon, NULL);

  return MGOS_APP_INIT_SUCCESS;
}
