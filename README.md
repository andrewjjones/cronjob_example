# ESP8266 Cronjob demo

## To install
mos build --arch esp8266

mos flash

## To test
mos call Cron.Add '{"at":"*/15 * * * * *", "action":"ledon"}'
Using port COM25
{
  "id": 1
}

mos call Cron.Add '{"at":"*/15 * * * * *", "action":"ledoff"}'
Using port COM25
{
  "id": 2
}
